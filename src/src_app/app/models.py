from sqlalchemy.dialects.postgresql import JSON
from sqlalchemy import  Column, Integer, JSON, DateTime
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

class Base(db.Model):

    __abstract__ = True
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Unicode(128))
    date_created  = db.Column(db.DateTime,  default=db.func.current_timestamp())



class Device(Base):

    __tablename__ = 'device'
    model = db.Column(db.Unicode(128))
    marca = db.Column(db.Unicode(128))
    function = db.Column(db.Unicode(128))



class DetaileDevice(Base):

    __tablename__ = 'detaile_device'
    device_id = db.Column(db.Integer, db.ForeignKey('device_id'))
    device = relationship('Device')
    component_id = db.Column(db.Integer, db.ForeignKey('component_id'))
    component = relationship('Component')




class Action:

    __tablename__ = 'action'
    id = db.Column(db.Integer, primary_key=True)
    date_created  = db.Column(db.DateTime,  default=db.func.current_timestamp())
    device_id = db.Column(db.Integer, db.ForeignKey('device_id'))
    device = relationship('Device')
    state = db.Column(db.Unicode(2)) #on/off




class Measure:

    __tablename__= 'measure'
    id = db.Column(db.Integer, primary_key=True)
    date_created  = db.Column(db.DateTime,  default=db.func.current_timestamp())
    device_id = db.Column(db.Integer, db.ForeignKey('device_id'))
    device = relationship('Device')
    med_sensor = db.Column(JSON)




class Component(Base):

    __tablename__ = 'component'
    model = db.Column(db.Unicode(128))
    marca = db.Column(db.Unicode(128))
    precision = db.Column(db.Unicode(128))
    variable = db.Column(db.Unicode(128))
    type_component = db.Column(db.Unicode(128))
    description = db.Column(db.Unicode(128))



class Cultivo(Base):

    __tablename__ = 'cultivo'
    hight = db.Column(db.Integer, primary_key=True)
    lenght = db.Column(db.Integer, primary_key=True)
    type_cultivo = db.Column(db.Unicode(128)) # hydro




class Huerta(Base):

    __tablename__ = 'huerta'
    hight = db.Column(db.Integer, primary_key=True)
    lenght = db.Column(db.Integer, primary_key=True)
    type_huerta = db.Column(db.Unicode(128)) # indoor/outdoor




class User(Base):

    __tablename__ = 'user'
    mail = db.Column(db.Unicode(128))
    password = db.Column(db.Unicode(128))




class Recipe(Base):

    __tablename__ = 'recipe'
    info_recipe = db.Column(JSON)





class DetaileCultivo(Base):

    __tablename__ = 'detaile_cultivo'
    cultivo_id = db.Column(db.Integer, db.ForeignKey('cultivo_id'))
    cultivo = relationship('Cultivo')
    plant_id = db.Column(db.Integer, db.ForeignKey('plant_id'))
    plant = relationship('Plant')
    cantidad = db.Column(db.Unicode(128))



class Book(Base):

    __tablename__ = 'base'
    recipe_id = db.Column(db.Integer, db.ForeignKey('recipe_id'))
    recipe = relationship('Recipe')




class Plant(Base):

    __tablename__ = 'plant'
    varieti_id = db.Column(db.Integer, db.ForeignKey('varieti_id'))
    varieti = relationship('Varieti')



class Specie(Base):

    __tablename__ = 'specie'
    especie_id = db.Column(db.Integer, db.ForeignKey('especie_id'))
    especie = relationship('Especie')




class Varieti(Base):

    __tablename__ = 'varieti'
    genotipo = db.Column(db.Unicode(128))
    cruce = db.Column(db.Unicode(128))
    production = db.Column(db.Unicode(128))
    specie_id = db.Column(db.Integer, db.ForeignKey('specie_id'))
    specie = relationship('Specie')




class CicloVegetativo(Base):

    __tablename__ = 'ciclo_vegetativo'
    plant_id = db.Column(db.Integer, db.ForeignKey('plant_id'))
    plant = relationship('Plant')




class UserCultivo(Base):

    __tablename__ = 'user_cultivo'
    role = db.Column(db.Unicode(128))
    last_conexion  = db.Column(db.DateTime)
    cultivo_id = db.Column(db.Integer, db.ForeignKey('cultivo_id'))
    cultivo = relationship('Cultivo')
    user_id = db.Column(db.Integer, db.ForeignKey('user_id'))
    user = relationship('User')





class DetaileDevice(Base):

    __tablename__ = 'detaile_device'
    device_id = db.Column(db.Integer, db.ForeignKey('device_id'))
    device = relationship('Device')





