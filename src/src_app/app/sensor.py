from serial import Serial
import os




class Huerta(object):
    nameHuerta = ""
    pass



class Controller(object):

    ports = []
    baud = ""

    def __init__(self, ports):
        self.ports = ports
        self.baud = ""
        self.arduino = Serial()
        self.instanciar()

    def instanciar(self):
        ports = self.ports
        if os.path.exists(ports[0]):
            fname = ports[0]
        else:
            fname = ports[1]

        try :
            self.arduino.port = fname
            self.arduino.open()
            self.on = True
            return True
        except:
            self.on = False
            print("Necesita conectar " + fname + " antes de seguir")
            return False




class Actuador(object):

    nameAct = ""
    pin = 0
    TYPE_ACCION_ON = 0
    TYPE_ACCION_OFF = 1


    def __init__(self, name="",  Controlador="", pin=0):
        self.nameAct = name
        self.controller = Controlador
        self.pin = pin

    def setAccion(self, accion):
        if accion == "on":
            print("accion:" + accion)
            self.setOn()
        else:
            self.setOff()


    def setOn(self):
        order = self.nameAct+ ":" + str(self.pin) + ":" + str(self.TYPE_ACCION_ON)
        print(order)
        self.controller.arduino.write(order.encode())
        return True

    def setOff(self):
        order = self.nameAct + ":" + str(self.pin) + ":" + str(self.TYPE_ACCION_OFF)
        print(order)
        self.controller.arduino.write(order.encode())
        return True




class Sensor(object):

    mediciones = {
            'TEMP' : 0,
            'HUM' : 0,
            'LUZ' : 0,
            'HUM_SOIL' : 0
            }

    ports = []
    baud = ""
    on = False

    def __init__(self, name, Controlador=""):
        self.nameAct = name
        self.controller = Controlador


    def read_data(self, accion):
        if accion == "medir:on":
            self.controller.arduino.write(accion.encode())
            mediciones = self.mediciones
            med = self.controller.arduino.readline().decode("utf-8")[:-2]
            med = med.split(',')
            for s in med:
                nom, val = s.split(':')
                mediciones[nom] = val
        return mediciones



