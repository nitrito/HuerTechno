from celery import Celery
import json
import time
from app.models import db
from app.sensor import Controller
from app.sensor import Actuador
from app.sensor import Sensor
from app.models import Mediciones


TIME_MED = 10


arduino_sensor = Controller(ports=['/dev/ttyUSB0', '/deb/ttyUSB1'])
arduino_actuador = Controller(ports=['/dev/ttyACM0', '/dev/ttyAMA0'])


regador = Actuador("REG", arduino_actuador, pin=6)
luces = Actuador("LUZ", arduino_actuador, pin=5)
aire = Actuador("FAN", arduino_actuador, pin=4)


S = Sensor("sensores", arduino_sensor)


BACKEND = BROKER = 'redis://localhost:6379'

celery = Celery(__name__, backend=BACKEND, broker=BROKER)


@celery.task(name='task.regar')
def regar(accion):
    regador.setAccion(accion)
    return "tarea de regar"


@celery.task(name='task.iluminar')
def iluminar(accion):
    luces.setAccion(accion)
    return "tarea de iluminar"


@celery.task(name='task.ventilar')
def fantask(accion):
    aire.setAccion(accion)
    return "tarea de ventilar"


@celery.task(name='task.medir')
def medir(accion):
    start = True
    while start:
        data_sensor = S.read_data(accion)
        mediciones = json.dumps(data_sensor)
        med_db = Mediciones(med_sensor=mediciones)
        print(mediciones)

        db.session.add(med_db)
        db.session.commit()
        time.sleep(TIME_MED)


