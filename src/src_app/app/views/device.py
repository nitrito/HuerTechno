from flask import Blueprint, render_template
import app.background as celery
import json
import time


device = Blueprint('sensor', __name__)


@device.route('/med/<accion>')
def med(accion):
    medir_task = celery.medir.delay(accion)
    return render_template('sensor/index.html')


@device.route('/make/<actuador>/<accion>')
def make(actuador, accion):
    if(actuador == "regar"):
        regar_task = regar.delay(accion)
    elif(actuador == "luz"):
        iluminar_task = iluminar.delay(accion)
    elif(actuador == "fan"):
        fan_task = fan.delay(accion)
    return render_template('sensor/index.html')





