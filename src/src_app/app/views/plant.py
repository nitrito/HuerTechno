from flask import Blueprint, render_template


plant = Blueprint('planta', __name__)


@plant.route('/planta')
def index():
    titulo = "Planta"
    return render_template('planta/index.html')

