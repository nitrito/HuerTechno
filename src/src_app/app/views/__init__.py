from .calendar import calendar
from .device import device
from .home import home
from .huerta import huerta
from .plant import plant

blueprints = [calendar, device, home, huerta, plant]
