from flask import Blueprint, render_template
from app.models import Mediciones
from app.models import db
import json
from datetime import datetime
from app.sensor import Sensor




#define blueprint
home = Blueprint('home', __name__)

mediciones = {
        'HORA':[0]*7,
        'TEMP':[0]*7,
        'HUM':[0]*7,
        'LUZ':[0]*7,
        'HUM_SOIL':[0]*7
        }



#set the route
@home.route('/')
def index():
    ''' Toma las últimas 5 mediciones '''
    #print(S.on)
    #if S.on == False:
    #   return render_template('error/coneccion.html')
    data = db.session.query(Mediciones).order_by(Mediciones.date_created)[-5:]
    for med in data:
        med_split = json.loads(med.med_sensor)
        date_med =  datetime.strftime(med.date_created, "%H:%M:%S")
        mediciones['HORA'].pop(0)
        mediciones['HORA'].append(date_med)
        for s,v in med_split.items():
            mediciones[s].pop(0)
            mediciones[s].append(v)
    print(mediciones)
    return render_template('home/index.html', data=json.dumps(mediciones))




