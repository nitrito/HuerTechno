  #include <DHT11.h>
/* 
 * HuerTechno v1.0
 * Prototipo serial
 * Fecha de actualización: 26-mar-2018
 * 
 * Este script recolecta las mediciones de los sensores instalados en el cultivo
 * Las mediciones son transmitidas mediante cable serial (USB) a la Raspberry
 * También se pueden enviar instrucciones desde la raspberry hasta el arduino
 * tales como manipular luz y agua.
 * 
 * Licencia Software Libre 
 *
 */


/*
* Definir variables de pin
*/

int DHT_PIN = 2; //DHT
int HUM_SOIL_PIN = A0; // yl-69
int LUZ_PIN = A1; // celda de luz
int RIEGO_PIN = 4; //rele bomba de riego
int LUCES_PIN = 5; //rele luz
int FAN_PIN = 6; //ventilador 1
int ACT_PIN;
/*
*Definir variables del sistema
*/
float lum;
float hum_soil;
float temp;
float hum;
String accion;
String nameAct="0";
String inAccion = "";
int state;



DHT11 dht11(DHT_PIN); 


void setup()
{
  
  Serial.begin(9600);
  delay(300);
  pinMode(RIEGO_PIN, OUTPUT);
  pinMode(LUCES_PIN, OUTPUT);  
  pinMode(FAN_PIN, OUTPUT);
  delay(700);
  
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }

  
}

void loop() {
  /*
   * Escucha mediante conexión serial, las ordenes deben ir 
   * sin salto de linea ni retorno de carro "\r\n"
   */
    //medir("medir:on");
    inAccion = accion_read();
    if(inAccion == "medir:on"){
      medir(inAccion);
    }else {
      actuador(inAccion);
    }
    
    delay(1000);
}


String accion_read(){
  inAccion = ""; 
  while (Serial.available() > 0) {
    int inChar = Serial.read();
    //Serial.println(inChar);
    inAccion += char(inChar); 
  }
  return inAccion;
}


int actuador(String accion){
    nameAct = accion.substring(0, 3);
    ACT_PIN = accion.substring(4, 5).toInt();
    state = accion.substring(6, 7).toInt();
    digitalWrite(ACT_PIN, state);   
    return 0; 
 }


void medir(String medir_accion){
    if(medir_accion == "medir:on"){
      lum = map(analogRead(LUZ_PIN), 0, 530, 0, 99);
      //lum = analogRead(LUZ_PIN);
      hum_soil = map(analogRead(HUM_SOIL_PIN), 200, 690, 99, 0);
      int error = dht11.read(hum, temp);
      if(error == 0){
        Serial.print("TEMP:");
        Serial.print(temp);
        Serial.print(",HUM:");
        Serial.print(hum);
        Serial.print(",LUZ:");
        Serial.print(lum);
        Serial.print(",HUM_SOIL:");
        Serial.println(hum_soil);
        delay(800);
      }
    }
}

  


 
