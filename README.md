---
output:
  html_document:
    df_print: paged
---
Tanto el código como la [documentación](https://0xacab.org/vladimir.aap/HuerTechno/wikis/home) son un trabajo en proceso.

por agregar:

- modificar el usuario en el manual de www-data a pi
- agregar la configuración on pass de postgress
- documentar el script de inicio de celery
- incorporar el arbol de carpetas
- incorporar las configuraciones para el servidor


# Instalación

Para ver los detalles del funcionamiento y componentes del Sistema de monitoreo y control remoto [TecnoHuerta v1.0](https://0xacab.org/tecnohuerta/HuerTechno/wikis/huertechno-v1.0) favor dirigirse al enlace. 


A continuación se presenta la instalación del sistema el cual está montado en una Raspberry pi con [Raspbian Stretch lite  2018-03-13](https://www.raspberrypi.org/downloads/raspbian/). 

Toda la instalación y configuración es mediante ssh, por lo que si necesitas saber como conectarte favor dirigirte a la pagina de la wiki sobre [configuración de internet y ssh]().

## I. Instalación de los servidores y bases de datos:

    1. En este momento debemos instalar el servidor Nginx, que serivirá de poroxy revers.

> `$ sudo apt-get install nginx `

    2. Posteriormente instalamos Redis, el cual servirá de broker junto a Celery.

> `$ sudo apt-get install redis-server`

    3. A continuación instalamos y configuramos la base de datos Postgresql

> `$ sudo apt-get install postgresql`

creamos la base de datos **huertechno** y el role **pi** a quien asociaremos a la base de datos y le daremos los privilegios correspondientes.

 > `$ sudo -u postgres bash`

Entramos al interprete de postgresql

> `postgres@raspberrypi:/home/pi$ psql`

Creamos la base de dato **huertechno** quien almacenará las mediciones desde los sensores.

>  `postgres=# create database tecnohuerta;`
  
Creamos el role **pi**  y le asignamos todos los privilegios

> `postgres=# CREATE USER "pi"; `
 
> `postgres=# GRANT ALL PRIVILEGES ON DATABASE "huertechno" to "pi";`

Salimos de *psql*

>  `postgres=# \q; `

## II. Instalación y configuración de directorios y librerías de python:

    1. Primero asegurarte que tu sistema está actualizado en la última versión
  
  >  `pi@raspberrypi:~ $ sudo apt-get update`
  
  > `pi@raspberrypi:~ $ sudo apt-get upgrade`


    2. Luego debemos instalar *pip* para instalar paquetes en python y *virtualenv* como entorno virtual. La razón es que nativamente se encuentra en el sistema la versión de python2.7, sin embargo, esta versión en poco tiempo quedará sin soporte por lo que se debe instalar python3. En este sentido, para no tener problemas en el sistema y romper dependencias generamos un entorno virtual.

> `pi@raspberrypi:~ $ sudo apt-get python-pip`

> `pi@raspberrypi:~ $ sudo pip install virtualenv`




    3. Creamos los directorios donde irá la aplicación y le damos permisos de grupo.

> `pi@raspberrypi:~ $ mkdir /home/pi/TecnoHuerta`

> `pi@raspberrypi:~ $ sudo chown -R www-data:www-data /home/pi/TecnoHuerta`

> `pi@raspberrypi:~ $ cd /home/pi/TecnoHuerta` 

    4. Instalamos Git y clonamos el repositorio de git dentro de la carpeta creada
    
> `pi@raspberrypi:~/TecnoHuerta $ sudo apt-get install -y git`

> `pi@raspberrypi:~/TecnoHuerta $ git clone https://0xacab.org/tecnohuerta/HuerTechno.git`


..pegar arbol de directorios ... 

    5. Una vez que tenemos el repositorio clonado procedemos a crear el entorno virtual e instalar las librerías necesarias de python.

> `pi@raspberrypi:~/TecnoHuerta $ virtualenv -p python3 venv `

> `pi@raspberrypi:~/TecnoHuerta $ source venv/bin/activate `

> `(venv) pi@raspberrypi:~/TecnoHuerta $ pip install -r requirements.txt`

Hasta este momento debemos tener el entorno virtual activado y los directorios creados. Para comprobar revisamos la versión de python

> `(venv) pi@raspberrypi:~/TecnoHuerta $ python -V`

> `Python 3.5.3`

## III. Instalamos uWSGI y configuramos Nginx:

    1. Instalamos uwsgi en el sistema y el plugin para la posterior configuración. 

> `$ sudo apt-get install uwsgi uwsgi-plugin-python3`

## IV. Permiso de acceso sobre el Puerto Serie:

    1. Agregamos al usuario del sistema operativo (en este caso: pi) al grupo dialout.

> `$ sudo usermod -a -G dialout pi`

....Se debe continuar con la configuraciones respectivas

## Docker

Chequea en que arquitectura ARM estás

```
uname -a
cat /proc/cpuinfo | grep model
``` 

Instalar docker

`curl -sSL https://get.docker.com | sh`

Permisos

```
sudo usermod -aG docker pi
echo "desloguearse y reloguearse para que se haga efectivo esto. si, posta."
```

Prueba que funciona

```
docker run armhf/hello-world
```

> [queda ver esto](https://iotbytes.wordpress.com/create-your-first-docker-container-for-raspberry-pi-to-blink-an-led/)

