FROM debian:9-slim


#RUN mkdir /home/src

WORKDIR   /src

RUN apt-get update  && \
    apt-get install  python3-pip  -y
   
ADD  /src /src

RUN pip3 install -r /src/src_app/requirements.txt

EXPOSE 80

CMD ["python3", "/src/src_app/run.py"]



